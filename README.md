- [English](#automatic-installation-of-nexus-with-ansible-on-proxmox)

# Automatische Installation von Nexus mit Ansibel auf Proxmox

## Inhaltsverzeichnis

- [Automatische Installation von Nexus mit Ansibel auf Proxmox](#automatische-installation-von-nexus-mit-ansibel-auf-proxmox)
  - [Inhaltsverzeichnis](#inhaltsverzeichnis)
  - [Über das Projekt](#über-das-projekt)
  - [Voraussetzungen](#voraussetzungen)
  - [Installation](#installation)
    - [Schritte](#schritte)
  - [Verwendung](#verwendung)
    - [Ansible Playbook ausführen](#ansible-playbook-ausführen)
    - [Tags](#tags)
  - [Variable Konfiguration](#variable-konfiguration)
    - [`vars.yml`](#varsyml)
      - [Allgemeine Variablen](#allgemeine-variablen)
      - [Variablen für Proxmox](#variablen-für-proxmox)
      - [Variablen für Proxmox Ubuntu Template](#variablen-für-proxmox-ubuntu-template)
      - [Variablen für Proxmox Ubuntu-Nexus VM](#variablen-für-proxmox-ubuntu-nexus-vm)
      - [Variablen für Nexus](#variablen-für-nexus)
      - [Variablen für Docker-Repo](#variablen-für-docker-repo)
      - [Variablen für Raw-Repo](#variablen-für-raw-repo)
      - [Variablen für Helm-Repo](#variablen-für-helm-repo)
      - [Variablen für Maven-Repo](#variablen-für-maven-repo)
    - [`vault.yml`](#vaultyml)
  - [SSL Konfiguration Nexus](#ssl-konfiguration-nexus)
    - [Beschreibung](#beschreibung)
    - [Spezielle Variablen für nexus-install](#spezielle-variablen-für-nexus-install)
    - [Zertifikat und Schlüsseldatei](#zertifikat-und-schlüsseldatei)
  - [Zusätzliche Konfigurationsdateien](#zusätzliche-konfigurationsdateien)
    - [ansible.cfg](#ansiblecfg)
      - [Sektion `[defaults]`](#sektion-defaults)
      - [Sektion `[ssh_connection]`](#sektion-ssh_connection)
  - [Lizenz](#lizenz)
- [Automatic Installation of Nexus with Ansible on Proxmox](#automatic-installation-of-nexus-with-ansible-on-proxmox)
  - [About the Project](#about-the-project)
  - [Prerequisites](#prerequisites)
  - [Installation](#installation-1)
    - [Steps](#steps)
  - [Usage](#usage)
    - [Executing Ansible Playbook](#executing-ansible-playbook)
    - [Tags](#tags-1)
  - [Variable Configuration](#variable-configuration)
    - [`vars.yml`](#varsyml-1)
      - [General Variables](#general-variables)
      - [Variables for Proxmox](#variables-for-proxmox)
      - [Variables for Proxmox Ubuntu Template](#variables-for-proxmox-ubuntu-template)
      - [Variables for Proxmox Ubuntu-Nexus VM](#variables-for-proxmox-ubuntu-nexus-vm)
      - [Variables for Nexus](#variables-for-nexus)
      - [Variables for Docker-Repo](#variables-for-docker-repo)
      - [Variables for Raw-Repo](#variables-for-raw-repo)
      - [Variables for Helm-Repo](#variables-for-helm-repo)
      - [Variables for Maven-Repo](#variables-for-maven-repo)
    - [`vault.yml`](#vaultyml-1)
  - [SSL Configuration Nexus](#ssl-configuration-nexus)
    - [Description](#description)
    - [Special Variables for nexus-install](#special-variables-for-nexus-install)
    - [Certificate and Key File](#certificate-and-key-file)
  - [Additional Configuration Files](#additional-configuration-files)
    - [ansible.cfg](#ansiblecfg-1)
      - [Section `[defaults]`](#section-defaults)
      - [Section `[ssh_connection]`](#section-ssh_connection)
  - [License](#license)

## Über das Projekt

Dieses Projekt verwendet Ansible, um eine Reihe von Aufgaben zu automatisieren:

- Erstellung eines Proxmox-Templates
- Einrichtung einer Nexus-VM
- Installation von Nexus auf der VM

## Voraussetzungen

- Installiertes Ansible
- Zugriff auf einen Proxmox-Host
- SSH-Zugriff auf die Ziele

## Installation

### Schritte

1. Klonen Sie dieses Repository.
2. Wechseln Sie in das Verzeichnis des Projekts.

```bash
cd auto-install-nexus-on-proxmox
```

## Verwendung

### Ansible Playbook ausführen

Um das Ansible-Playbook zu starten, nutzen Sie den folgenden Befehl:

```bash
ansible-playbook --ask-vault-pass site.yml --tags "complete_install"
```

**⚠️ Achtung ⚠️**

Bevor Sie das Ansible-Playbook ausführen, stellen Sie sicher, dass Sie die Datei `ansible.cfg` und die Variablen in `vars.yml` und `vault.yml` entsprechend Ihren Anforderungen angepasst haben.

### Tags

Im Projekt sind verschiedene Tags definiert, um bestimmte Teile des Playbooks gezielt auszuführen:

- `complete_install`: Führt alle Rollen und Tasks im Playbook aus.
- `create_template`: Nur die Rolle `proxmox-template` wird ausgeführt.
- `install_nexus_vm`: Die Rollen `proxmox-vm` & `nexus-install` werden ausgeführt.
- `create_nexus_vm`: Nur die Rolle `proxmox-vm` wird ausgeführt.
- `install_nexus`: Nur die Rolle `nexus-install` wird ausgeführt.

## Variable Konfiguration

Die Konfigurationsvariablen sind in zwei Dateien unterteilt: `vars.yml` für allgemeine Konfigurationsdaten und `vault.yml` für sensitive Daten. Hier sind die Details zu den Variablen, die in diesen Dateien definiert sind:

### `vars.yml`

#### Allgemeine Variablen

| Variable      | Beispiel-Wert   | Beschreibung                  |
|---------------|----------------|-------------------------------|
| `gateway`     | `192.168.1.1`| Gateway-Adresse für das Netzwerk. |
| `dns_server`  | `192.168.1.2`| DNS-Serveradresse für das Netzwerk. |
| `domain`      | `example.de`    | Domain-Name für das Netzwerk. |

#### Variablen für Proxmox

| Variable            | Beispiel-Wert       | Beschreibung                       |
|---------------------|--------------------|------------------------------------|
| `proxmox_api_host`  | `192.168.1.230`  | Proxmox API Hostadresse.           |
| `proxmox_api_user`  | `admin@pve`        | Proxmox API Benutzername.          |
| `proxmox_node`      | `proxmox`          | Name des Proxmox-Nodes.            |

#### Variablen für Proxmox Ubuntu Template

| Variable                  | Beispiel-Wert               | Beschreibung                               |
|---------------------------|----------------------------|--------------------------------------------|
| `template_id`             | `10000`                    | Template-ID in Proxmox.                    |
| `template_name`           | `Template-Ubuntu-22.04-Cloud`| Name des Templates.                      |
| `template_image_code_name`| `jammy`                    | Ubuntu-Version Codename für das Template.  |
| `template_hdd_size`       | `200G`                     | Festplattengröße des Templates.            |

#### Variablen für Proxmox Ubuntu-Nexus VM

| Variable      | Beispiel-Wert      | Beschreibung                  |
|---------------|-------------------|-------------------------------|
| `vm_id`       | `1003`            | VM-ID in Proxmox.             |
| `vm_hostname` | `nexus2.example.de`| Vollqualifizierter Hostname der VM. |
| `vm_name`     | `nexus2`          | Name der VM.                  |
| `vm_user`     | `dev`             | Benutzername für die VM.      |
| `vm_ip`       | `192.168.1.234` | IP-Adresse der VM.            |
| `vm_cores`    | `4`               | Anzahl der CPU-Kerne für die VM. |
| `vm_memory`   | `4096`            | Arbeitsspeicher in MB für die VM. |
| `vm_hdd`      | `400`             | Festplattengröße in GB für die VM. |

#### Variablen für Nexus

| Variable              | Beispiel-Wert       | Beschreibung                             |
|-----------------------|--------------------|------------------------------------------|
| `java_version`        | `openjdk-8-jre-headless`| Java-Version für Nexus.             |
| `nexus_version`       | `3.60.0-02`         | Nexus-Version.                           |
| `nexus_user`          | `nexus`             | Benutzername für Nexus.                  |
| `nexus_install_path`  | `/opt/nexus`       | Installationspfad für Nexus.             |
| `nexus_data_path`     | `/opt/sonatype-work`| Datenpfad für Nexus.                   |
| `nexus_heap_size`     | `4096m`            | Heap-Speichergröße für Nexus.            |
| `nexus_domain_name`   | `nexus2.example.de` | Domain-Name für Nexus.                   |
| `nexus_gui_user`      | `admin`            | GUI-Benutzername für Nexus.              |
| `nexus_admin_first_name`| `Max`        | Vorname des Nexus-Admins.                |
| `nexus_admin_last_name`| `Mustermann`   | Nachname des Nexus-Admins.               |
| `anonymous_access`    | `false`            | Anonymen Zugriff in Nexus zulassen oder nicht. |

#### Variablen für Docker-Repo

| Variable                       | Beispiel-Wert  | Beschreibung                                         |
|--------------------------------|---------------|------------------------------------------------------|
| `docker_repo`                  | `true`         | Docker-Repository aktivieren oder deaktivieren.      |
| `docker_repo_name`             | `mm-docker`    | Name des Docker-Repositories.                        |
| `docker_repo_port`             | `8085`         | Port für das Docker-Repository.                      |
| `docker_repo_v1`               | `false`        | Docker-Repository V1-Protokollunterstützung.         |
| `docker_repo_policy`           | `allow`        | Bereitstellungspolitik für das Docker-Repository.    |
| `docker_repo_strict_validation`| `true`         | Strikte Validierung für das Docker-Repository.       |

#### Variablen für Raw-Repo

| Variable                       | Beispiel-Wert  | Beschreibung                                         |
|--------------------------------|---------------|------------------------------------------------------|
| `raw_repo`                     | `true`         | Raw-Repository aktivieren oder deaktivieren.         |
| `raw_repo_name`                | `mm-raw`       | Name des Raw-Repositories.                           |
| `raw_repo_policy`              | `allow`        | Bereitstellungspolitik für das Raw-Repository.       |
| `raw_repo_strict_validation`   | `true`         | Strikte Validierung für das Raw-Repository.          |
| `raw_repo_content_disposition` | `ATTACHMENT`   | Inhaltsanordnung für das Raw-Repository.             |

#### Variablen für Helm-Repo

| Variable                       | Beispiel-Wert  | Beschreibung                                         |
|--------------------------------|---------------|------------------------------------------------------|
| `helm_repo`                    | `true`         | Helm-Repository aktivieren oder deaktivieren.        |
| `helm_repo_name`               | `mm-helm`      | Name des Helm-Repositories.                          |
| `helm_repo_policy`             | `allow`        | Bereitstellungspolitik für das Helm-Repository.      |
| `helm_repo_strict_validation`  | `true`         | Strikte Validierung für das Helm-Repository.         |

#### Variablen für Maven-Repo

| Variable                       | Beispiel-Wert  | Beschreibung                                         |
|--------------------------------|---------------|------------------------------------------------------|
| `maven_repo`                   | `false`        | Maven-Repository aktivieren oder deaktivieren.       |
| `maven_repo_name`              | `mm-maven`     | Name des Maven-Repositories.                         |
| `maven_repo_strict_validation` | `true`         | Strikte Validierung für das Maven-Repository.        |
| `maven_repo_policy`            | `allow`        | Bereitstellungspolitik für das Maven-Repository.     |
| `maven_repo_version_policy`    | `SNAPSHOT`     | Versionierungspolitik für das Maven-Repository.      |
| `maven_repo_layout_policy`     | `STRICT`       | Layoutpolitik für das Maven-Repository.              |
| `maven_repo_content_disposition`| `ATTACHMENT`  | Inhaltsanordnung für das Maven-Repository.           |

### `vault.yml`

| Variable              | Beispiel-Wert   | Beschreibung                        |
|-----------------------|----------------|-------------------------------------|
| `proxmox_api_password`| `Test123!`     | Passwort für Proxmox API.           |
| `vm_password`         | `Test123!`     | Passwort für die VM.                |
| `become_pass`         | `Test123!`     | Passwort für den `become`-Befehl in Ansible. |
| `local_password`      | `Test123!`    | Lokales Passwort.                   |
| `nexus_password`      | `Test123!`     | Passwort für Nexus.                 |
| `nexus_admin_email`   | `max@example.de`| E-Mail-Adresse des Nexus-Admins. |
| `sshkey`              | `ssh-rsa`      | SSH-Schlüssel.                      |

## SSL Konfiguration Nexus

### Beschreibung

Diese Rolle kümmert sich um die Installation von Nexus auf einer Ubuntu-basierten VM. Sie setzt voraus, dass bereits eine VM existiert, auf der Nexus installiert werden soll.

### Spezielle Variablen für nexus-install

| Variable              | Beispielwert      | Beschreibung                    |
|-----------------------|-------------------|---------------------------------|
| `vm_name`             | `nexus`         | Name der VM für Nexus          |
| `domain`              | `example.de`       | Domain der VM                   |

### Zertifikat und Schlüsseldatei

Für die Installation werden ein SSL-Zertifikat und ein Schlüssel benötigt. Diese sollten in folgenden Formaten und Pfaden verfügbar sein:

- Zertifikatsdatei: `nexus.example.de.crt`
- Schlüsseldatei: `nexus.example.de.key`

Diese Dateien sollten im Ordner `roles/nexus-install/files/` abgelegt werden.

**⚠️ Achtung ⚠️**
Wenn Sie keine Zertifikate hinterlegen, wird automatisch ein selbstsigniertes Zertifikat erstellt.

## Zusätzliche Konfigurationsdateien

### ansible.cfg

Die `ansible.cfg`-Datei enthält grundlegende Einstellungen für die Ausführung von Ansible. Hier eine kurze Erklärung der wichtigsten Parameter:

#### Sektion `[defaults]`

- `inventory`: Pfad zur Inventardatei. Standardmäßig auf `./inventory.ini` gesetzt.
- `remote_user`: Benutzername für den SSH-Zugang. In diesem Projekt ist es `dev`.
- `private_key_file`: Pfad zum privaten SSH-Schlüssel. Hier ist es `~/.ssh/<Private_Key_Name>`.
- `log_path`: Pfad zur Log-Datei für Ansible-Aktionen. Hier ist es `./ansible.log`.
- `host_key_checking`: Überprüfung des SSH-Host-Schlüssels deaktivieren. Eingestellt auf `False` (nicht empfohlen für Produktivumgebungen).

#### Sektion `[ssh_connection]`

- `ssh_args`: Zusätzliche SSH-Optionen. Hier ist `-o HostKeyAlgorithms=+ssh-rsa` eingestellt, um den verwendeten HostKeyAlgorithmus explizit zu spezifizieren.

## Lizenz

Dieses Projekt steht unter der MIT-Lizenz - siehe die [LICENSE.md](LICENSE.md) Datei für weitere Details.


---

# Automatic Installation of Nexus with Ansible on Proxmox

## About the Project

This project utilizes Ansible to automate a series of tasks:

- Creation of a Proxmox template
- Setup of a Nexus VM
- Installation of Nexus on the VM

## Prerequisites

- Installed Ansible
- Access to a Proxmox host
- SSH access to the targets

## Installation

### Steps

1. Clone this repository.
2. Change to the project directory.

```bash
cd auto-install-nexus-on-proxmox
```

## Usage

### Executing Ansible Playbook

To start the Ansible Playbook, use the following command:

```bash
ansible-playbook --ask-vault-pass site.yml --tags "complete_install"
```

**⚠️ Caution ⚠️**

Before executing the Ansible Playbook, ensure that you have adjusted the file `ansible.cfg` and the variables in `vars.yml` and `vault.yml` according to your requirements.

### Tags

Various tags are defined in the project to execute specific parts of the playbook selectively:

- `complete_install`: Executes all roles and tasks in the playbook.
- `create_template`: Only the role `proxmox-template` is executed.
- `install_nexus_vm`: The roles `proxmox-vm` & `nexus-install` are executed.
- `create_nexus_vm`: Only the role `proxmox-vm` is executed.
- `install_nexus`: Only the role `nexus-install` is executed.

## Variable Configuration

The configuration variables are divided into two files: `vars.yml` for general configuration data and `vault.yml` for sensitive data. Here are the details of the variables defined in these files:

### `vars.yml`

#### General Variables

| Variable      | Example Value   | Description                  |
|---------------|----------------|-------------------------------|
| `gateway`     | `192.168.1.1`| Gateway address for the network. |
| `dns_server`  | `192.168.1.2`| DNS server address for the network. |
| `domain`      | `example.com`    | Domain name for the network. |

#### Variables for Proxmox

| Variable            | Example Value       | Description                       |
|---------------------|--------------------|------------------------------------|
| `proxmox_api_host`  | `192.168.1.230`  | Proxmox API host address.           |
| `proxmox_api_user`  | `admin@pve`        | Proxmox API username.               |
| `proxmox_node`      | `proxmox`          | Name of the Proxmox node.           |

#### Variables for Proxmox Ubuntu Template

| Variable                  | Example Value               | Description                               |
|---------------------------|----------------------------|--------------------------------------------|
| `template_id`             | `10000`                    | Template ID in Proxmox.                    |
| `template_name`           | `Template-Ubuntu-22.04-Cloud`| Name of the template.                   |
| `template_image_code_name`| `jammy`                    | Ubuntu version codename for the template.  |
| `template_hdd_size`       | `200G`                     | Hard drive size of the template.           |

#### Variables for Proxmox Ubuntu-Nexus VM

| Variable      | Example Value      | Description                  |
|---------------|-------------------|-------------------------------|
| `vm_id`       | `1003`            | VM ID in Proxmox.             |
| `vm_hostname` | `nexus2.example.com`| Fully qualified hostname of the VM. |
| `vm_name`     | `nexus2`          | Name of the VM.               |
| `vm_user`     | `dev`             | Username for the VM.          |
| `vm_ip`       | `192.168.1.234` | IP address of the VM.         |
| `vm_cores`    | `4`               | Number of CPU cores for the VM. |
| `vm_memory`   | `4096`            | Memory in MB for the VM.      |
| `vm_hdd`      | `400`             | Hard drive size in GB for the VM. |

#### Variables for Nexus

| Variable              | Example Value       | Description                             |
|-----------------------|--------------------|------------------------------------------|
| `java_version`        | `openjdk-8-jre-headless`| Java version for Nexus.            |
| `nexus_version`       | `3.60.0-02`         | Nexus version.                           |
| `nexus_user`          | `nexus`             | Username for Nexus.                      |
| `nexus_install_path`  | `/opt/nexus`       | Installation path for Nexus.             |
| `nexus_data_path`     | `/opt/sonatype-work`| Data path for Nexus.                   |
| `nexus_heap_size`     | `4096m`            | Heap memory size for Nexus.              |
| `nexus_domain_name`   | `nexus2.example.com` | Domain name for Nexus.                 |
| `nexus_gui_user`      | `admin`            | GUI username for Nexus.                  |
| `nexus_admin_first_name`| `Max`        | First name of Nexus admin.               |
| `nexus_admin_last_name`| `Mustermann`   | Last name of Nexus admin.                |
| `anonymous_access`    | `false`            | Allow or disallow anonymous access in Nexus. |

#### Variables for Docker-Repo

| Variable                       | Example Value  | Description                                         |
|--------------------------------|---------------|------------------------------------------------------|
| `docker_repo`                  | `true`         | Enable or disable Docker repository.                 |
| `docker_repo_name`             | `mm-docker`    | Name of Docker repository.                           |
| `docker_repo_port`             | `8085`         | Port for the Docker repository.                      |
| `docker_repo_v1`               | `false`        | Docker repository V1 protocol support.               |
| `docker_repo_policy`           | `allow`        | Deployment policy for the Docker repository.         |
| `docker_repo_strict_validation`| `true`         | Strict validation for the Docker repository.         |

#### Variables for Raw-Repo

| Variable                       | Example Value  | Description                                         |
|--------------------------------|---------------|------------------------------------------------------|
| `raw_repo`                     | `true`         | Enable or disable Raw repository.                    |
| `raw_repo_name`                | `mm-raw`       | Name of Raw repository.                              |
| `raw_repo_policy`              | `allow`        | Deployment policy for the Raw repository.            |
| `raw_repo_strict_validation`   | `true`         | Strict validation for the Raw repository.            |
| `raw_repo_content_disposition` | `ATTACHMENT`   | Content disposition for the Raw repository.          |

#### Variables for Helm-Repo

| Variable                       | Example Value  | Description                                         |
|--------------------------------|---------------|------------------------------------------------------|
| `helm_repo`                    | `true`         | Enable or disable Helm repository.                   |
| `helm_repo_name`               | `mm-helm`      | Name of Helm repository.                             |
| `helm_repo_policy`             | `allow`        | Deployment policy for the Helm repository.           |
| `helm_repo_strict_validation`  | `true`         | Strict validation for the Helm repository.           |

#### Variables for Maven-Repo

| Variable                       | Example Value  | Description                                         |
|--------------------------------|---------------|------------------------------------------------------|
| `maven_repo`                   | `false`        | Enable or disable Maven repository.                  |
| `maven_repo_name`              | `mm-maven`     | Name of Maven repository.                            |
| `maven_repo_strict_validation` | `true`         | Strict validation for the Maven repository.          |
| `maven_repo_policy`            | `allow`        | Deployment policy for the Maven repository.          |
| `maven_repo_version_policy`    | `SNAPSHOT`     | Versioning policy for the Maven repository.          |
| `maven_repo_layout_policy`     | `STRICT`       | Layout policy for the Maven repository.              |
| `maven_repo_content_disposition`| `ATTACHMENT`  | Content disposition for the Maven repository.        |

### `vault.yml`

| Variable              | Example Value   | Description                        |
|-----------------------|----------------|-------------------------------------|
| `proxmox_api_password`| `Test123!`     | Password for Proxmox API.           |
| `vm_password`         | `Test123!`     | Password for the VM.                |
| `become_pass`         | `Test123!`     | Password for the `become` command in Ansible. |
| `local_password`      | `Test123!`    | Local password.                     |
| `nexus_password`      | `Test123!`     | Password for Nexus.                 |
| `nexus_admin_email`   | `max@example.com`| Email address of Nexus admin.   |
| `sshkey`              | `ssh-rsa`      | SSH key.                            |

## SSL Configuration Nexus

### Description

This role takes care of the installation of Nexus on an Ubuntu-based VM. It assumes that a VM already exists on which Nexus is to be installed.

### Special Variables for nexus-install

| Variable              | Example Value      | Description                    |
|-----------------------|-------------------|---------------------------------|
| `vm_name`             | `nexus`         | Name of the VM for Nexus        |
| `domain`              | `example.com`       | Domain of the VM               |

### Certificate and Key File

For the installation, an SSL certificate and a key are required. These should be available in the following formats and paths:

- Certificate file: `nexus.example.com.crt`
- Key file: `nexus.example.com.key`

These files should be placed in the `roles/nexus-install/files/` folder.

**⚠️ Caution ⚠️**
If you do not provide certificates, a self-signed certificate will be created

 automatically.

## Additional Configuration Files

### ansible.cfg

The `ansible.cfg` file contains basic settings for executing Ansible. Here is a brief explanation of the most important parameters:

#### Section `[defaults]`

- `inventory`: Path to the inventory file. Default is set to `./inventory.ini`.
- `remote_user`: Username for SSH access. In this project, it is `dev`.
- `private_key_file`: Path to the private SSH key. Here it is `~/.ssh/<Private_Key_Name>`.
- `log_path`: Path to the log file for Ansible actions. Here it is `./ansible.log`.
- `host_key_checking`: Disable SSH host key checking. Set to `False` (not recommended for production environments).

#### Section `[ssh_connection]`

- `ssh_args`: Additional SSH options. Here it is set to `-o HostKeyAlgorithms=+ssh-rsa` to explicitly specify the used HostKeyAlgorithm.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for more details.
